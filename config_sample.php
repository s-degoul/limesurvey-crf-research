<?php

/* Minimum adaptations = replace "my_" values by suitable ones */



/* Mark config as loaded */
define("CONFIG_LOADED", 1);

/* Mode: dev or prod */
define("MODE", "prod");

/* LimeSurvey */
/* Root URL */
define("LS_ROOT_URL", "my_LS_URL");
/* URL of back office */
define("LS_ADMIN_URL", "my_LS_backend_URM");
/* Survey ID */
define("LS_SURVEY_ID", "my_LS_survey_ID");
/* API features */
/* User: preferably with limited privileges!! */
define("LS_API_USER", "my_LS_user");
define("LS_API_PASSWORD", "my_LS_password");
define("LS_API_URL", LS_ROOT_URL."admin/remotecontrol");
/* Question code of question corresponding to subject name initials (optional) */
define("LS_Q_SUBJECT_NAME_INITIALS", "my_initials_question_code");
/* Question code corresponding to subject ID and ID prefix in questionnaire (optional) */
define("LS_Q_SUBJECT_ID", "my_id_question_code");
define("LS_Q_SUBJECT_ID_PREFIX", "my_id_question_prefix");

/* Format of dates */
/* - in user interface, complete */
define("DATE_FORMAT_UI", "d/m/Y H\hi");
/* - in user interface, short */
define("DATE_FORMAT_UI_SHORT", "d/m/Y");
/* - in logs */
define("DATE_FORMAT_LOG", "Y-m-d H:i:s");
/* - in file name */
define("DATE_FORMAT_FILE", "Y.m.d_H.i.s");


/* Files */
/* - data directory */
define("DIR_DATA", "data/");
/* - subjects list */
define("FILE_SUBJECTS_LIST", DIR_DATA."subjects_list.csv");
/* - backups */
define("FILE_BACKUP_NAME_FORMAT", "backup_{date}.json");
/* - logs */
define("FILE_LOG", "data/logs");

/* Separator for CSV files */
define("SEP_CSV", ";");

/* Variables in subjects list file */
/* - subject id */
define("SUBJECTS_LIST_VAR_ID", "id");
/* - common name for variables relative to token  */
define("SUBJECTS_LIST_VAR_SURVEY_TOKEN", "survey_token");
/* - comment */
define("SUBJECTS_LIST_VAR_COMMENT", "comment");
/* - group (optional) */
define("SUBJECTS_LIST_VAR_GROUP", "group");

/* ID format (regex) for test subjects */
define("TEST_SUBJECT_ID_REGEX", "#^my_study_number-0[0-9]{2}$#");

/* Emails */
/* Addresses must be compatible with PHP mail() function https://www.php.net/manual/en/function.mail.php */
/* Name of expeditor */
define("EMAIL_FROM_NAME", "my_email_from_name");
/* Email address of expeditor */
define("EMAIL_FROM_ADDRESS", "my_email_from_address");
/* Email address to reply to */
define("EMAIL_REPLY_TO", "my_email_reply_to_address");
/* Email address(es) to send alerts to */
define("EMAILS_ALERTS_TO", "my_alerts_recipients_mail_concatenated_with_comma");
/* Email address(es) to send alerts about inclusions to */
define("EMAILS_ALERTS_INCLUSION_TO", "my_inclusion_alerts_recipients_mail_concatenated_with_comma");


/* Users */
$users = array(
    array(
        "name" => "my_user_1_name",
        "username" => "my_user_1_login",
        "password" => "my_user_1_password",
        "rights" => array(
            "launch_dm" => TRUE,
        ),
    ),
);
