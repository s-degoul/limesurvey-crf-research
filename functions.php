<?php
if(! defined("CONFIG_LOADED")) {
    http_response_code(403);
    die("Forbidden");
}

/**
 * Load dependencies
 */
include_once("vendor/autoload.php");


/**
 * Class auto loading
 */
spl_autoload_register(function ($class) {
    $class_file = "classes/".$class.".php";
    if (! file_exists($class_file)){
        throw new Exception("Unable to load class ".$class);
    }
    include_once($class_file);
});


/**
 * Print variable for debug purpose
 * @param   mixed   $a
 * @return  none
 */
function print_output($a){
    if (MODE != "dev"){
        return;
    }
    if (is_array($a) or is_object($a)){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }
    else {
        echo $a."<br/>";
    }
}


/**
 * Register a log in FILE_LOG file
 * @param   string  $log_message    Information to log
 * @param   string  $log_level      Log level, among ERROR, WARNING, NOTICE
 * @return  int
 */
function add_log(string $log_message, string $log_level = "ERROR"){
    if (! in_array($log_level, array("ERROR", "WARNING", "NOTICE"))){
        $log_level = "unknown";
    }
    $f = @fopen(FILE_LOG, "a+");
    if (! $f){
        die("Unable to write in log file ".FILE_LOG);
    }
    $now = new DateTime();
    fwrite($f, "[".$log_level."] ".$now->format(DATE_FORMAT_LOG)." - ".$log_message.PHP_EOL);
    fclose($f);
    return 0;
}


/**
 * Replace spaces with non-breakable spaces of a string
 * @param   string  $input  input string
 * @return  string          output string
 */
function non_breakable_spaces($input){
    return str_replace(" ", "&nbsp;", $input);
}



/*************************/
/* Data management files */
/*************************/


/**
 * Get all available data management backup file present in DIR_DATA named according to FILE_BACKUP_NAME_FORMAT config
 * @param   none
 * @return  array   $dm_files   List of filenames
 */
function get_dm_files(){
    $files = scandir(DIR_DATA);
    $dm_files = array();
    foreach ($files as $file){
        if (preg_match("#^".str_replace("{date}", "(.+)", FILE_BACKUP_NAME_FORMAT)."$#", $file, $result)){
            $dm_files[] = array(
                "file" => DIR_DATA.$file,
                "date" => date_create_from_format(DATE_FORMAT_FILE, $result[1])
            );
        }
    }
    return $dm_files;
}

/**
 * Get data management backup file at a given date
 * @param   string  $date  date of the backup, formated according to DATE_FORMAT_FILE config or "last"
 * @return  string  $dm_file    filename of backup file
 */
function get_dm_file(string $date){
    if ($date == "last"){
        $file_date = null;
        $dm_file = null;
        foreach(get_dm_files() as $file){
            if (is_null($file_date)
                or $file["date"] > $file_date){
                $file_date = $file["date"];
                $dm_file = $file["file"];
            }
        }
    }
    else {
        $dm_file = DIR_DATA.str_replace("{date}", $date, FILE_BACKUP_NAME_FORMAT);
        if (! file_exists($dm_file)){
            throw new Exception("File ".$dm_file." doesn't exist");
        }
    }
    return $dm_file;
}


/**
 * Get a setter method for an attribute
 * @param   string  $attr   Name of attribute
 * @param   string  $method_type    Type of method: "set" (attribute's value is an object or builtin type) or "add" (attribute's value is an array)
 * @param   string  $class  Class name
 * @return  string  $method Method name
 */
function get_method_from_attr(string $attr, string $method_type, string $class){
    $method = $method_type.str_replace("_", "", ucwords($attr, "_"));
    if ($method_type == "add"
        and substr($method, -1) == "s"){
        $method = substr($method, 0, -1);
    }
    if (! method_exists($class, $method)){
        throw new Exception("Unknown method ".$method." in class ".$class);
    }
    return $method;
}

/**
 * Get a class from attribute name of another class
 * @param   string  $attr   Attribute name
 * @param   boolean $plural Is attribute name in plural form? (with an trailing "s")
 * @return  string  $class  Class name
 */
function get_class_from_attr(string $attr, bool $plural = false){
    /* Special handling of some attributes of DateTime type
     * HINT: set in config? Compute automatically? */
    if (in_array($attr, array("date", "start_date", "end_date", "completion_date"))){
        return "DateTime";
    }
    $class = str_replace("_", "", ucwords($attr, "_"));
    if ($plural
        and substr($class, -1) == "s"){
        $class = substr($class, 0, -1);
    }
    if (! class_exists($class)){
        throw new Exception("Unknown class ".$class);
    }
    return $class;
}


/**
 * Convert an array to an object of a given class
 * @param   array   $input  Array of key/value with:
 * - key = class attribute or integer in case of array of objects
 * - value = value of attribute (can be an array)
 * @param   string  $class  Name of the class
 * @return  object  $obj    A object instance of class $class
 */
function array_to_object(array $input, string $class){
    if (! class_exists($class)){
        throw new Exception("Unknown class ".$class);
    }
    /* Special handling of DateTime object.
     * $input["date"] should contain string version of date ready to use by DateTime constructor */
    else if ($class == "DateTime"){
        return new DateTime($input["date"]);
    }
    $obj = new $class;
    foreach ($input as $key => $value){
        /* If no value, no setter function called */
        if (! $value){
            continue;
        }
        /* $value is array */
        if (is_array($value) and ! empty($value)){
            /* key is integer => $value is an array of objects
             * "add" method called for each object */
            if (is_integer(array_keys($value)[0])){
                $add_method = get_method_from_attr($key, "add", $class);
                foreach ($value as $val){
                    $obj->$add_method(array_to_object($val, get_class_from_attr($key, true)));
                }
            }
            /* key is an attribute name => $value is one object
             * "set" method called for this object */
            else {
                $set_method = get_method_from_attr($key, "set", $class);
                $obj->$set_method(array_to_object($value, get_class_from_attr($key)));
            }
        }
        /* $value is not an array => can be "set" to attribute as is */
        else {
            $set_method = get_method_from_attr($key, "set", $class);
            $obj->$set_method($value);
        }
    }
    return $obj;
}


/**
 * Load json-formated data from a previous data management
 * from a backup file
 * @param   string  $date   date of the backup, formated according to DATE_FORMAT_FILE config or "last"
 * @return  array   $result array of 4 arrays with Subject / Appointment / Survey / Alert objects, respectively
 */
function load_dm(string $date){
    $result = array(
        "subjects" => array(),
        "surveys" => array(),
        "alerts" => array()
    );

    $dm = array();
    $dm_file = get_dm_file($date);
    if ($dm_file){
        $dm = json_decode(file_get_contents($dm_file), true);
    }
    if (empty($dm)){
        return $result;
    }

    foreach ($result as $data_type => $dummy){
        $class = get_class_from_attr($data_type, true);
        foreach($dm[$data_type] as $dm_object_data){
            $result[$data_type][] = array_to_object($dm_object_data, $class);
        }
    }

    return $result;
}


/**
 * Save data management result (subjects list) in a json format
 * in a backup file named according to FILE_BACKUP_NAME_FORMAT config
 * @param   array   $subjects       List of Subject objects
 * @param   array   $surveys        List of Survey objects
 * @param   array   $alerts         List of Alert objects
 * @return  int
 */
function save_dm(array $subjects, array $surveys, array $alerts){
    $dm = array(
        "subjects" => $subjects,
        "surveys" => $surveys,
        "alerts" => $alerts
    );
    $result = json_encode($dm);

    try {
        $dm_file = DIR_DATA.str_replace("{date}", date(DATE_FORMAT_FILE), FILE_BACKUP_NAME_FORMAT);
        $f = fopen($dm_file, "w");
        fwrite($f, $result);
        fclose($f);
    }
    catch (Exception  $e){
        add_log("Unable to save data management result in file ".$dm_file.": ".$e->getMessage());
        exit(1);
    }
    return 0;
}





/*****************/
/* Subjects list */
/*****************/

/**
 * Import subjects list CSV file content
 * @param   none
 * @return  array   $file_content
 */
function import_subjects_list_file(){
    /* Expected variables: relative to id, number and surveys' token */
    $expected_variables = array(SUBJECTS_LIST_VAR_ID, SUBJECTS_LIST_VAR_SURVEY_TOKEN, SUBJECTS_LIST_VAR_COMMENT);
    if (defined("SUBJECTS_LIST_VAR_GROUP")){
        $expected_variables[] = SUBJECTS_LIST_VAR_GROUP;
    }

    try {
        $f = fopen(FILE_SUBJECTS_LIST, "r");
        $variables = array();
        $file_content = array();

        $i = 0;
        $values_nb_first_line = 0;
        while ($line = fgetcsv($f, 1000, SEP_CSV)){
            $i ++;
            $values_nb = count($line);
            /* First line = variables' name */
            if ($i == 1){
                $values_nb_first_line = $values_nb;
                for($j = 0; $j < $values_nb; $j++){
                    $pos = array_keys($expected_variables, $line[$j]);
                    if (count($pos) != 1){
                        continue;
                    }
                    $variables[$j] = $line[$j];
                    unset($expected_variables[$pos[0]]);
                }
                if (count($expected_variables)){
                    throw new Exception("Some variables are missing: "
                                        .implode(", ", $expected_variables));
                }
                continue;
            }
            /* Other lines = values */
            if ($values_nb != $values_nb_first_line){
                throw new Exception("Line ".$i.": wrong number of values (expected ".$values_nb_first_line.")");
            }
            $file_content[] = array();
            for ($j = 0; $j < $values_nb; $j++){
                if (in_array($j, array_keys($variables))){
                    $file_content[count($file_content) - 1][$variables[$j]] = $line[$j];
                }
            }
        }
        fclose($f);
    }
    catch (Exception  $e){
        add_log("Unable to import subjects list file: ".$e->getMessage());
        exit(1);
    }

    return $file_content;
}

/**
 * Load subjects info from CSV subjects list and merging with previous data management
 * @param   array   $subjects   List of Subject objects
 * @param   array   $alerts     List of Alert objects
 * @return  int
 */
function load_subjects_list(array &$subjects, array &$alerts){
    /* File content and number of rows */
    $subjects_list_file_content = import_subjects_list_file();
    $subjects_list_file_content_nb = count($subjects_list_file_content);
    /* Subjects already exist from previous data management? */
    $subjects_exist_previous_dm = ! empty($subjects);

    /* Loop throw each CSV row */
    for ($i = 0; $i < $subjects_list_file_content_nb; $i++){
        /* Subject ID defined in SUBJECTS_LIST_VAR_ID variable of CSV array */
        $subject_id = $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_ID];

        /* Test if subject already exists in previous data management */
        $subject_exists = false;
        foreach ($subjects as $subject){
            if ($subject->getId() == $subject_id){
                $subject_exists = true;
                break;
            }
        }
        /* Creating new subject if not already exists */
        if (! $subject_exists){
            $subject = new Subject();
            $subject->setId($subject_id);
            /* Alert, only if subjects list already imported from previous data management */
            if ($subjects_exist_previous_dm){
                $subject->createAlert(new Alert(
                    Alert::LEVEL_WARNING,
                    "new_subject_in_subjects_list",
                    "Un nouveau sujet est apparu : identifiant = ".$subject->getId()),
                                      $alerts);
            }
            /* FIXME: subject id in array key usefull? */
            $subjects[$subject->getId()] = $subject;
        }

        /* Creating/checking survey */
        $survey_token = $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_SURVEY_TOKEN];
        if (! $subject->getSurvey()){
            /* Alert if survey not present in already existing subject */
            if ($subject_exists){
                $subject->createAlert(new Alert(
                    Alert::LEVEL_ERROR,
                    "survey_previously_absent",
                    "Sujet ".$subject->getCompleteId()." : aucun questionnaire n'existait dans la version précédente du data management"),
                                      $alerts);
            }

            $survey = new Survey();
            $subject->setSurvey($survey);
        }
        /* Alert if survey token has changed */
        else if ($subject->getSurvey()->getToken() != $survey_token){
            $subject->createAlert(new Alert(
                Alert::LEVEL_ERROR,
                "survey_token_changed",
                "Sujet ".$subject->getCompleteId()." : le token du questionnaire était ".$subject->getSurvey()->getToken()." et est maintenant ".$survey_token),
                                  $alerts);
        }
        $subject->getSurvey()->setToken($survey_token);

        /* Adding comment */
        $subject->setComment($subjects_list_file_content[$i][SUBJECTS_LIST_VAR_COMMENT]);
        /* Adding group */
        if (defined("SUBJECTS_LIST_VAR_GROUP")){
            $subject->setGroup($subjects_list_file_content[$i][SUBJECTS_LIST_VAR_GROUP]);
        }
    }

    /* Search for previously existing subjects which are no more present in the subjects list
     * and alert if so, but remove this subject FIXME:doc */
    foreach ($subjects as $key => $subject){
        $subject_exists = false;
        for ($i = 0; $i < $subjects_list_file_content_nb; $i++){
            if ($subject->getId() == $subjects_list_file_content[$i][SUBJECTS_LIST_VAR_ID]){
                $subject_exists = true;
            }
        }
        if (! $subject_exists){
            $alerts[] = new Alert(
                Alert::LEVEL_ERROR,
                "subject_deleted",
                "Le sujet ".$subject->getId()." n'existe plus dans la liste des sujets");
            unset($subjects[$key]);
        }
    }

    return 0;
}






/*******************/
/* LimeSurvey data */
/*******************/


/**
 * Import LimeSurvey data through API
 * @param   none
 * @return  array   $result["responses"]    List of "control" information about responses (dates, token...)
 */
function import_LS_data(){
    try {
        /* Using a RPC client */
        $myJSONRPCClient = new \org\jsonrpcphp\JsonRPCClient(LS_API_URL);
        $sessionKey= $myJSONRPCClient->get_session_key(LS_API_USER, LS_API_PASSWORD);

        /* FIXME: Limiting API resquest would be great (10th argument of export_responses()),
         * but requests uses SGQA identifiers whereas returned data are named with question code!  */

        /* $items = array("id", "token", "startdate", "submitdate"); */
        /* /\* Adding question about subject initials, if configured /!\ must be the last value of $items array *\/ */
        /* if (defined("LS_Q_SUBJECT_NAME_INITIALS")){ */
        /*     $items[] = LS_Q_SUBJECT_NAME_INITIALS; */
        /* } */
        /* if (defined("LS_Q_SUBJECT_ID")){ */
        /*     $items[] = LS_Q_SUBJECT_ID; */
        /* } */

        $result = $myJSONRPCClient->export_responses($sessionKey, LS_SURVEY_ID, "json", null, "all", "code", "long");
        $myJSONRPCClient->release_session_key($sessionKey);

        /* No answers => return null */
        if (is_array($result)){
            return null;
        }

        $result = json_decode(base64_decode($result), true);

        /* Data in one nested array whose key is "responses" */
        return $result["responses"];
    }
    catch (Exception  $e){
        add_log("Unable to import surveys responses with API: ".$e->getMessage());
        exit(1);
    }
}

/**
 * Make a list of Survey objects from LimeSurvey data
 * @param   none
 * @return  array   $surveys    List of Survey objects
 */
function load_surveys(){
    $surveys = array();

    $surveys_data = import_LS_data();

    if ($surveys_data){
        try {
            foreach ($surveys_data as $survey_data){
                if (! is_array($survey_data) or count($survey_data) != 1){
                    throw new Exception("Survey data badly formated: ".serialize($survey_data));
                }
                /* Data present in one nested array whose key = survey's ID */
                $survey_data = reset($survey_data);

                $survey = new Survey();

                $survey->setId($survey_data["id"]);

                /* Missing token means incorrect LimeSurvey configuration */
                if (! $survey_data["token"]){
                    throw new Exception("No token for survey id=".$survey->getId());
                }

                $survey->setToken($survey_data["token"]);

                if ($survey_data["startdate"]){
                    $survey->setStartDate(new DateTime($survey_data["startdate"]));
                }

                if ($survey_data["submitdate"]){
                    $survey->setCompletionDate(new DateTime($survey_data["submitdate"]));
                }
                /* Complete status attribute from dates */
                $survey->setStatusFromDates();

                if (defined("LS_Q_SUBJECT_NAME_INITIALS")){
                    if (! array_key_exists(LS_Q_SUBJECT_NAME_INITIALS, $survey_data)){
                        throw new Exception("No field ".LS_Q_SUBJECT_NAME_INITIALS);
                    }
                    $survey->setSubjectNameInitials($survey_data[LS_Q_SUBJECT_NAME_INITIALS]);
                }
                if (defined("LS_Q_SUBJECT_ID")){
                    if (! array_key_exists(LS_Q_SUBJECT_ID, $survey_data)){
                        throw new Exception("No field ".LS_Q_SUBJECT_ID);
                    }
                    if ($survey_data[LS_Q_SUBJECT_ID]){
                        $subject_id_prefix = LS_Q_SUBJECT_ID_PREFIX ? LS_Q_SUBJECT_ID_PREFIX : "";
                        $survey->setSubjectId($subject_id_prefix.$survey_data[LS_Q_SUBJECT_ID]);
                    }
                }

                $surveys[] = $survey;
            }
        }
        catch (Exception $e){
            add_log("Error in surveys loading: ".$e->getMessage());
            exit(1);
        }
    }

    return $surveys;
}




/**********************************/
/* Multiple sources data handling */
/**********************************/


/**
 * Add surveys information to a subject
 * @param   array   $surveys    List of Survey objects
 * @param   Subject $subject    A Subject object
 * @param   array   $alerts     List of Alert objects
 * @return  int
 */
function add_survey_subject(array &$surveys, Subject &$subject, array &$alerts){
    $survey_token = $subject->getSurvey()->getToken();
    $survey_added = false;
    /* Add survey data, if exists */
    foreach ($surveys as $key => $survey){
        if ($survey->getToken() == $survey_token){
            $survey_status = $survey->getStatus();
            /* Alert if inconsistent status change */
            if ($survey_status < $subject->getSurvey()->getStatus()){
                $subject->createAlert(new Alert(
                    Alert::LEVEL_WARNING,
                    "survey_inconsistent_status_change",
                    "Sujet ".$subject->getCompleteId()." : le statut du questionnaire a changé d'une manière incohérente ; était \"".$subject->getSurvey()->getStatusDescription()."\" et est maintenant \"".$survey->getStatusDescription()."\""),
                                      $alerts);
            }

            /* Checking subject ID */
            $subject_id = $survey->getSubjectId();
            if ($subject_id and $subject_id != $subject->getId()){
                /* Alert if subject ID in survey is wrong */
                $subject->createAlert(new Alert(
                    Alert::LEVEL_WARNING,
                    "subject_wrong_id",
                    "Sujet ".$subject->getCompleteId()." : le numéro sujet dans le questionnaire n'est pas correct : \"".$subject_id."\""),
                                      $alerts);
            }

            /* Checking initials */
            $new_subject_initials = $survey->getSubjectNameInitials();
            $previous_subject_initials = $subject->getInitials();
            /* Apply new initials */
            $subject->setInitials($new_subject_initials);
            if ($new_subject_initials and ! $previous_subject_initials){
                /* Alert about new inclusion */
                $alerts[] = new Alert(
                    Alert::LEVEL_NOTICE,
                    "new_inclusion",
                    "Nouvelle inclusion",
                    "Nouvelle inclusion dans l'étude PREEMS du participant n° ".$subject->getCompleteId()
                    ." le ".$survey->getStartDate()->format(DATE_FORMAT_UI_SHORT),
                    null,
                    EMAILS_ALERTS_INCLUSION_TO);
            }
            else if ($new_subject_initials != $previous_subject_initials){
                /* Alert if initials have changed */
                $subject->createAlert(new Alert(
                    Alert::LEVEL_WARNING,
                    "subject_initials_changed",
                    "Sujet ".$subject->getCompleteId()." : les initiales ont été modifiées ; étaient \"".$previous_subject_initials."\" et sont maintenant \"".$new_subject_initials."\""),
                                      $alerts);
            }

            /* HINT: test if something has changed would be interesting, for further functionality? */
            $subject->setSurvey($survey);
            unset($surveys[$key]);
            $survey_added = true;
            break;
        }
    }
    /* Remove data from previously loaded survey, which doesn't exist anymore */
    if (! $survey_added &  $subject->getSurvey()->getStatus() > Survey::STATUS_NOT_DONE){
        /* Alert */
        $subject->createAlert(new Alert(
            Alert::LEVEL_WARNING,
            "survey_deleted",
            "Sujet ".$subject->getCompleteId()." : les données précédentes du questionnaires n'existent plus"),
                              $alerts);
        $survey = new Survey();
        $survey->setToken($survey_token);
        $subject->setSurvey($survey);
    }

    return 0;
}


/**
 * Complete subjects information with appointments and surveys lists
 * @param   array   $subjects       List of Subject objects
 * @param   array   $surveys        List of Survey objects
 * @param   array   $alerts         List of Alert objects
 * @return  int
 */
function complete_subjects_info(array &$subjects, array &$surveys, array &$alerts){
    foreach ($subjects as $subject){
        add_survey_subject($surveys, $subject, $alerts);
    }

    /* Alert if non-attached surveys */
    foreach ($surveys as $survey){
        $alerts[] = new Alert(
            Alert::LEVEL_ERROR,
            "survey_unattached",
            "Le questionnaire avec le token ".$survey->getToken()." n'est associé à aucun sujet");
    }

    return 0;
}



/*******************/
/* Alerts handling */
/*******************/

function send_alerts(array $alerts, array $subjects){
    usort($alerts, function($alert1, $alert2){
        if ($alert1->getLevel() != $alert2->getLevel()){
            return ($alert1->getLevel() > $alert2->getLevel()) ? -1 : 1;
        }
        return ($alert1->getDate() < $alert2->getDate()) ? -1 : 1;
    });

    $emails = array();

    foreach ($alerts as $alert){
        /* DEBUG */
        /* print_output($alert->__toString()); */

        $recipients = $alert->getRecipients();
        if ($recipients and $alert->getMessage()){
            if (! array_key_exists($recipients, $emails)){
                $emails[$recipients] = array();
            }
            $emails[$recipients][] = "*".ucfirst($alert->getLevelDescription())."* : ".$alert->getMessage();
        }
    }

    foreach ($emails as $recipients => $messages){
        $email_subject = "Etude PREEMS : notification importante";
        $email_message = "Bonjour.\r\n\r\n"
                       ."Ci-dessous une alerte déclenchée par l'algorithme de vérification des données de l'étude PREEMS.\r\n\r\n- "
                       .implode("\r\n\r\n- ", $messages);

        $email_header = "From: ".EMAIL_FROM_NAME." <".EMAIL_FROM_ADDRESS.">\r\n"
                      ."Reply-To: ".EMAIL_REPLY_TO."\r\n"
                      ."Content-Type: text/plain; charset=utf-8\r\n"
                      ."Content-Transfer-Encoding: quoted-printable";

        $email_content = quoted_printable_encode($email_message);

        /* Sending mail only if MODE = prod. Otherwise, display mail */
        if (MODE == "dev"){
            print_output(array($recipients, $email_subject, $email_content, $email_header));
        }
        else {
            if (! mail($recipients, $email_subject, $email_content, $email_header)) {
                add_log("Alert email sending to ".$recipients." failed");
            }
        }
    }

    return 0;
}


/***********/
/* Display */
/***********/

/**
 * Compute integer value from first numbers contained in an ID
 * @param   string   $id input ID
 * @return  int
 */
function get_int_from_id($id){
    preg_match("#[0-9]+#", $id, $result);
    if (! $result[0]){
        add_log("No integer ID can be compute from id ".$id);
        return 0;
    }
    return intval($result[0]);
}
