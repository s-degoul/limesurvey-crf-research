<?php

if (php_sapi_name() != "cli") {
    session_start();
    if (! isset($_SESSION["username"])){
        header("Location:login.php");
        exit;
    }
    if (! $_SESSION["rights"]["launch_dm"]){
        http_response_code(403);
        die("Forbidden");
    }
}

include_once("config.php");
include_once("functions.php");

/* List of new alerts */
$alerts = array();

/* Loading data */

/* - result of previous data management */
$subjects = load_dm("last")["subjects"];

/* - subjects list (fixed) */
load_subjects_list($subjects, $alerts);
/* print_output($subjects); */

/* - surveys from LimeSurvey */
$surveys = load_surveys();
/* print_output($surveys); */

/* Add surveys information */
complete_subjects_info($subjects, $surveys, $alerts);

/* Save result of data management */
save_dm($subjects, $surveys, $alerts);

/* DEBUG */
/* print_output($subjects); */
/* print_output($surveys); */
/* print_output($alerts); */

/* Send alerts */
send_alerts($alerts, $subjects);

if (php_sapi_name() != "cli") {
    $_SESSION["message"] = "Data management réalisé";
    if (MODE != "dev"){
        header("Location:index.php");
    }
    else {
        echo "<p><a href=\"index.php\">Go to index</a></p>";
    }
}

exit(0);
