<?php
session_start();
if (! isset($_SESSION["username"])){
    header("Location:login.php");
    exit;
}

include_once("config.php");
include_once("functions.php");

/* Loading data from data management */
$dm_date = "last";
if (isset($_REQUEST["submit"])){
    $dm_date = stripslashes($_REQUEST["dm_date"]);
}
$dm = load_dm($dm_date);

/* Ordering information */
/* Subjects: by Number */
usort($dm["subjects"], function($subject1, $subject2){
    return ($subject1->getId() < $subject2->getId()) ? -1 : 1;
});
/* Alerts: by level & date */
usort($dm["alerts"], function($alert1, $alert2){
    if ($alert1->getLevel() != $alert2->getLevel()){
        return ($alert1->getLevel() > $alert2->getLevel()) ? -1 : 1;
    }
    return ($alert1->getDate() < $alert2->getDate()) ? -1 : 1;
});


/* DEBUG */
/* print_output($dm); */

include("header.html");
?>

<div class="row">
    <div class="col-sm-6">
        <p>
            <strong>Utilisateur :</strong> <?php echo $_SESSION["name"]; ?>.
            <a href="logout.php">Se déconnecter</a>
        </p>
    </div>
    <div class="col-sm-6">

<?php
if (empty($dm["subjects"])){
?>
        <p class="alert alert-info">
            <span class="fa-solid fa-circle-info"></span> Pas de <i>data-management</i> disponible
        </p>
<?php
    if ($_SESSION["rights"]["launch_dm"]){
?>
        <p>
            <a href="data_management.php" onclick="return confirm('Lancer un DM ? Ceci enverra les éventuelles alertes.')">
            <span class="fa-solid fa-rotate"></span> Lancer un <i>data-management</i>
            </a>
        </p>
    </div>
</div>
<?php
    }
    include("footer.html");
    exit;
}

if (isset ($_SESSION["message"]) and ! empty($_SESSION["message"])){
?>
        <p class="alert alert-info">
            <span class="fa-solid fa-envelope-circle-check"></span> <?php echo $_SESSION["message"]; ?>
        </p>
<?php
    unset($_SESSION["message"]);
}
?>


<?php
$dm_files = get_dm_files();
usort($dm_files, function($dm_file1, $dm_file2){
    if($dm_file1["date"] == $dm_file2["date"]){
        return 0;
    }
    return ($dm_file1["date"] > $dm_file2["date"]) ? -1 : 1;
});
?>
        <form action="" method="post">
            <details data-popover="bottom">
                <summary>
<?php
$i = 0;
$dm_file_date = "";
foreach ($dm_files as $dm_file){
    $i ++;
    if ($dm_date == $dm_file["date"]->format(DATE_FORMAT_FILE)
        or ($dm_date == "last" and $i == 1)){
        $dm_file_date = $dm_file["date"]->format(DATE_FORMAT_UI);
        break;
    }
}
?>
                    <strong>Date du <i>data-management</i>&nbsp;:</strong> <?php echo $dm_file_date; ?>
                </summary>
                <div>
                    <div class="form-group row">
                        <label for="dm_date">Changer la date</label>
                        <div class="col-md-6">
                            <select name="dm_date" class="form-select">
<?php
$i = 0;
foreach ($dm_files as $dm_file){
    $i ++;
    $selected = "";
    if ($dm_date == $dm_file["date"]->format(DATE_FORMAT_FILE)
        or ($dm_date == "last" and $i == 1)){
        $selected = " selected";
    }
?>
                                <option value="<?php echo ($i == 1) ? "last" : $dm_file["date"]->format(DATE_FORMAT_FILE); ?>" <?php echo $selected; ?>>
                                    <?php echo $dm_file["date"]->format(DATE_FORMAT_UI); ?>
                                </option>
<?php
}
?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success" name="submit" value="1">
                                OK
                            </button>
                        </div>

<?php
if ($_SESSION["rights"]["launch_dm"]){
?>
                        <p>
                            <a href="data_management.php" onclick="return confirm('Lancer un DM ? Ceci enverra les éventuelles alertes.')">
                                <span class="fa-solid fa-rotate"></span> Lancer un <i>data-management</i>
                            </a>
                        </p>
<?php
}
?>
                    </div>
                </div>
            </details>
        </form>

    </div>
</div>


<?php
if ($dm_date != "last"){
?>
<p class="alert alert-info">
    <span class="fa-solid fa-circle-info"></span> Vous consultez les données d&apos;un data-management ancien
</p>
<?php
}
?>



<!--
--------
Subjects
--------
-->

<h2>Sujets</h2>

<div class="row">
    <div class="col-md-3">
        <button type="button" class="btn btn-primary" value="1" id="toggle-display-subjects">
            <span id="toggle-display-subjects-icon" class="fa-solid fa-eye-slash"></span> <span id="toggle-display-subjects-label">Masquer les sujets non inclus</span>
        </button>
    </div>
    <div class="col-md-3">
        <details data-popover="bottom" class="legend">
            <summary>
                Légende
            </summary>
            <div>
                <dl>
                    <dt>Questionnaire</dt>
                        <dd>
                            <ul>
<?php
        foreach(Survey::getConstants() as $constant_name => $constant_value){
            if (preg_match("#^STATUS_#", $constant_name)){
                $css = Survey::getPublicStatusCSS($constant_value);
?>
                <li class="<?php echo $css["text"]; ?>">
                    <span class="fa-solid <?php echo $css["icon"]; ?>"></span> <?php echo Survey::getPublicStatusDescription($constant_value); ?>
                </li>
<?php
            }
        }
?>
                            </ul>
                        </dd>

                    <dt>Divers</dt>
                    <dd>
                        <ul>
                            <li>
                                <span class="fa-solid fa-circle-plus"></span> cliquer pour afficher des détails
                            </li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </details>
    </div>
    <div class="col-md-3">
        <details data-popover="bottom" class="legend">
            <summary>
                Statistiques
            </summary>
<?php
$nb_inclusions = 0;
$nb_visits = array();
foreach ($dm["subjects"] as $subject){
    if (preg_match(TEST_SUBJECT_ID_REGEX, $subject->getId())){
        continue;
    }
    if ($subject->isIncluded()){
        $nb_inclusions ++;
    }
}
?>
            <div>
                <p><small>En excluant les sujets test (format du numéro sujet : <?php echo TEST_SUBJECT_ID_REGEX; ?>)</small></p>
                <dl>
                    <dt>Nombre d&apos;inclusions</dt>
                    <dd>
                        <?php echo $nb_inclusions; ?>
                    </dd>
                </dl>
            </div>
        </details>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-sm">
        <thead class="table-dark">
            <tr>
                <th scope="col">Numéro</th>
                <th scope="col">Initiales</th>
                <th scope="col">Groupe</th>
                <th scope="col">Commentaires</th>
                <th scope="col">Recueil de données</th>
                <th scope="col">Alertes</th>
            </tr>
        </thead>
        <tbody>
<?php
foreach ($dm["subjects"] as $subject){
    /* Highlighting subjects with alerts in recent data management */
    $css_row = "";
    $css_icon = "";
    $max_alert_level = 0;
    foreach ($subject->getAlerts() as $alert){
        if (in_array($alert, $dm["alerts"])){
            if ($alert->getLevel() > $max_alert_level){
                $max_alert_level = $alert->getLevel();
                $css_row = str_replace("alert", "bg", $alert->getLevelCSS()["text"]);
                $css_icon = "fa-solid ".$alert->getLevelCSS()["icon"];
            }
        }
    }
    if (! $subject->isIncluded()){
        $css_row .= " subject-not-included";
    }
?>
            <tr class="<?php echo $css_row; ?>">
                <th scope="row" class="subject-id" title="double-cliquez pour (dé)sélectionner"><?php echo $subject->getId(); ?><br/><span class="fa-solid fa-highlighter highlight-subject"></span><span class="<?php echo $css_icon ?>"></span></th>
                <th scope="row"><?php echo $subject->getInitials(); ?></th>
                <td class="subject-group">
                    <?php echo $subject->getGroup(); ?>
                </td>
                <td class="subject-comment"><?php echo $subject->getComment(); ?></td>
<?php


    $survey = $subject->getSurvey();
    if (! $survey){
        /* Should never happen. Log error */
        add_log("No survey found for subject ".$subject->getId());
        $css = Survey::getPublicStatusCSS(null);
        $status_description = Survey::getPublicStatusDescription(null);
        $survey_info = "";
    }
    else {
        $css = $survey->getStatusCSS();
        $status_description = $survey->getStatusDescription();
        $survey_info = "<ul><li>Token&nbsp;:&nbsp;".$survey->getToken()."</li><li>"
                     ."Début : ";
        if ($survey->getStartDate()){
            $survey_info .= $survey->getStartDate()->format(DATE_FORMAT_UI);
        }
        $survey_info .= "</li><li>Fin : ";
        if ($survey->getCompletionDate()){
            $survey_info .= $survey->getCompletionDate()->format(DATE_FORMAT_UI);
        }
        $survey_info .= "</li></ul>";
        $survey_info = non_breakable_spaces($survey_info);
    }
?>
                <td class="subject-survey">
                    <span class="fa-solid <?php echo $css['icon']; ?> <?php echo $css['text']; ?>" title="<?php echo $status_description; ?>"></span>&nbsp;
<?php
    if ($survey->getStatus() != Survey::STATUS_COMPLETED){
?>
                    <a href="<?php echo $survey->getURL(); ?>" target="_blank">Questionnaire</a>
<?php
    }
    else {
?>
                    <span class="inactive" title="Le questionnaire n'est plus accessible">Questionnaire</span>
<?php
    }
?>
                    <details data-popover="bottom">
                        <summary class="display-subject-survey-info">
                            <span class="fa-solid fa-circle-plus" title="Afficher les info du questionnaire"></span>
                        </summary>
                        <div class="subject-survey-info">
                            <?php echo $survey_info; ?>
                        </div>
                    </details>
                </td>

                <td class="subject-alerts">
<?php
if ($subject->getAlerts()){
        $alerts_info = "<ul>";
        foreach($subject->getAlerts() as $alert){
            $css = $alert->getLevelCSS();
            $alerts_info .= "<li class=\"alert ".$css["text"]."\"><strong><span class=\"fa-solid "
                         .$css["icon"]."\" title=\"".$alert->getLevelDescription()."\"></span> "
                         .$alert->getDate()->format(DATE_FORMAT_UI)
                         ." | ".$alert->getType()
                         ."</strong> : ".$alert->getDescription();
            if ($alert->getMessage()){
                $alerts_info .= "<br/><strong>Message : </strong>".$alert->getMessage();
            }
            $alerts_info .= "</li>\n";
        }
        $alerts_info .= "</ul>";
?>
                    <details data-popover="left">
                        <summary class="display-alerts-info">
                              <span class="fa-solid fa-circle-plus" title="Afficher les alertes"></span>&nbsp;détail
                        </summary>
                        <div class="alerts-info">
                            <?php echo $alerts_info; ?>
                        </div>
                    </details>
<?php
}
?>
                </td>
            </tr>
<?php
}
?>

        </tbody>
    </table>
</div>


<!--
--------
Orphan surveys
--------
-->

<?php
if ($dm["surveys"]){
?>
<h2>Questionnaires orphelins</h2>
<p>
    Questionnaires non attachés à un sujet
</p>
<p class="alert alert-danger">
    Il s'agit d'une anomalie.
</p>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="table-dark">
            <tr>
                <th scope="col">Token</th>
                <th scope="col">Date de début</th>
                <th scope="col">Date de fin</th>
                <th scope="col">Statut</th>
            </tr>
        </thead>
        <tbody>
<?php
    foreach ($dm["surveys"] as $survey){
        $start_date = "";
        if ($survey->getStartDate()){
            $start_date = $survey->getStartDate()->format(DATE_FORMAT_UI);
        }
        $completion_date = "";
        if ($survey->getCompletionDate()){
            $completion_date = $survey->getCompletionDate()->format(DATE_FORMAT_UI);
        }
?>
            <tr>
                <td><?php echo $survey->getToken(); ?></td>
                <td><?php echo $start_date; ?></td>
                <td><?php echo $completion_date; ?></td>
                <td><?php echo $survey->getStatusDescription(); ?></td>
            </tr>
<?php
    }
?>
        </tbody>
    </table>
</div>
<?php
}
?>


<!--
--------
Alerts
--------
-->

<h2>Alertes</h2>

<p>
    Alertes générées lors du <i>data management</i>.
</p>


<?php
if ($dm["alerts"]){
?>
<p>
    Nombre = <?php echo count($dm["alerts"]); ?>
</p>
<?php
    foreach ($dm["alerts"] as $alert){
        $css = $alert->getLevelCSS();
?>
<div class="alert <?php echo $css["text"]; ?>">
    <strong>
    <span class="fa-solid <?php echo $css["icon"]; ?>" title="<?php echo $alert->getLevelDescription(); ?>"></span>
    <?php echo $alert->getDate()->format(DATE_FORMAT_UI); ?> |
    Code = <?php echo $alert->getType(); ?> :
    </strong>
    <?php echo $alert->getDescription(); ?>
    <dl>
<?php
    if ($alert->getMessage()){
?>
        <dt>Message :</dt><dd><?php echo $alert->getMessage(); ?></dd>
<?php
    }
?>
    </dl>
</div>
<?php
    }
}
else {
?>
<p class="alert alert-success">
    Aucune
</p>
<?php
}
?>



<script type="text/javascript">
const el_toggle_display_subjects = document.getElementById("toggle-display-subjects");
const el_toggle_display_subjects_icon = document.getElementById("toggle-display-subjects-icon");
const el_toggle_display_subjects_label = document.getElementById("toggle-display-subjects-label");
toggle_display_subjects = function(action = "show"){
    if (action == "show"){
        el_toggle_display_subjects.value = 1;
        el_toggle_display_subjects_icon.classList.remove("fa-eye");
        el_toggle_display_subjects_icon.classList.add("fa-eye-slash");
        el_toggle_display_subjects_label.textContent = "Masquer les sujets non inclus";
    }
    else {
        el_toggle_display_subjects.value = 0;
        el_toggle_display_subjects_icon.classList.remove("fa-eye-slash");
        el_toggle_display_subjects_icon.classList.add("fa-eye");
        el_toggle_display_subjects_label.textContent = "Afficher les sujets non inclus";
    }
    const el_subject_rows = document.querySelectorAll(".subject-not-included");
    const el_subject_rows_l = el_subject_rows.length;
    let i;
    for (i = 0; i < el_subject_rows_l; i ++) {
        if (action == "show"){
            el_subject_rows[i].style.display = "";
        }
        else {
            el_subject_rows[i].style.display = "none";
        }
    };
}

el_toggle_display_subjects.addEventListener("click", function(){
    if (el_toggle_display_subjects.value == 1){
        toggle_display_subjects("hide");
    }
    else {
        toggle_display_subjects("show");
    }
});
toggle_display_subjects("hide");

const el_subject_numbers = document.querySelectorAll(".subject-id");
el_subject_numbers.forEach(function(el){
    el.addEventListener("dblclick", function(ev){
        el_row = ev.currentTarget.parentNode;
        if (el_row.classList.contains("subject-highlighted")){
            el_row.classList.remove("subject-highlighted");
        }
        else {
            el_row.classList.add("subject-highlighted");
        }
    })
});
</script>

<?php
include("footer.html");
