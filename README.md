
Helper program to use [LimeSurvey](https://www.limesurvey.org) questionnaire as case report form (CRF).

# Features

- data management script intended to run periodically (e.g. with a cron job)
- web dashboard

