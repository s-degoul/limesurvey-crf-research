<?php

class Survey implements JsonSerializable {
    private $id;
    private $token;
    private $start_date;
    private $completion_date;
    private $status;
    private $subject_name_initials;
    private $subject_id;

    public const STATUS_NOT_DONE = 1;
    public const STATUS_STARTED = 2;
    public const STATUS_COMPLETED = 3;

    public function __construct(){
        $this->setStatus(self::STATUS_NOT_DONE);
    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function __toString(){
        return "id".$this->getId()." token ".$this->getToken." started on ".$this->getStartDate()->format(DATE_FORMAT_LOG);
    }

    public function getId(){
        return $this->id;
    }

    public function setId(string $id){
        $this->id = $id;
    }

    public function getToken(){
        return $this->token;
    }

    public function setToken(string $token){
        $this->token = $token;
    }

    public function getStartDate(){
        return $this->start_date;
    }

    public function setStartDate(DateTime $start_date){
        $this->start_date = $start_date;
    }

    public function getCompletionDate(){
        return $this->completion_date;
    }

    public function setCompletionDate(DateTime $completion_date){
        $this->completion_date = $completion_date;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getStatusDescription(){
        return self::getPublicStatusDescription($this->status);
    }

    static public function getPublicStatusDescription($status){
        switch ($status){
        case SELF::STATUS_NOT_DONE:
            return "non débuté";
        case SELF::STATUS_STARTED:
            return "débuté";
        case SELF::STATUS_COMPLETED:
            return "complété";
        default:
            return "inconnu";
        }
    }

    public function getStatusCSS(){
        return self::getPublicStatusCSS($this->status);
    }

    static public function getPublicStatusCSS($status){
        $css = array(
            "icon" => "",
            "text" => ""
        );
        switch ($status){
        case self::STATUS_NOT_DONE:
            $css["icon"] = "fa-question";
            $css["text"] = "not started";
            break;
        case self::STATUS_STARTED:
            $css["icon"] = "fa-clock";
            $css["text"] = "ongoing";
            break;
        case self::STATUS_COMPLETED:
            $css["icon"] = "fa-check";
            $css["text"] = "completed";
            break;
        default:
            $css["icon"] = "fa-circle-question";
            $css["text"] = "unknown";
        }
        return $css;
    }

    public function setStatus(int $status){
        if (! in_array($status, self::getConstants())){
            throw new Exception("Survey ".$this->__toString().": unknown input status ".$status);
        }
        else {
            $this->status = $status;
        }
    }

    public function setStatusFromDates(){
        if ($this->getStartDate()){
            if ($this->getCompletionDate()){
                $this->setStatus(self::STATUS_COMPLETED);
            }
            else {
                $this->setStatus(self::STATUS_STARTED);
            }
        }
        else {
            $this->setStatus(self::STATUS_NOT_DONE);
        }
    }

    public function getSubjectNameInitials(){
        return $this->subject_name_initials;
    }

    public function setSubjectNameInitials(string $subject_name_initials){
        $this->subject_name_initials = str_replace(" ", "", trim(strtoupper(iconv("utf8", "ascii//TRANSLIT", $subject_name_initials))));
    }

     public function getSubjectId(){
        return $this->subject_id;
    }

    public function setSubjectId(string $subject_id){
        $this->subject_id = $subject_id;
    }

    public function getURL(){
        if (! $this->getToken()){
            throw new Exception("Survey ".$this->__toString().": no token information to make an URL");
        }
        return LS_ROOT_URL.LS_SURVEY_ID."?token=".$this->getToken();
    }

    static public function getConstants(){
        $reflectionClass = new ReflectionClass(__CLASS__);
        return $reflectionClass->getConstants();
    }
}
