<?php
class Subject implements JsonSerializable {
    private $id;
    private $initials;
    private $group;
    private $comment;
    private $survey;
    private $alerts;

    public function __construct(){

    }

    public function jsonSerialize() {
        return (object) get_object_vars($this);
    }

    public function __toString(){
        return $this->getId();
    }

    public function getId(){
        return $this->id;
    }

    public function setId(string $id){
        $this->id = $id;
    }

    public function getInitials(){
        return $this->initials;
    }

    public function setInitials(string $initials){
        $this->initials = $initials;
    }

    public function getCompleteId(){
        $initials = $this->getInitials();
        if ($initials){
            return $this->getId()."-".$initials;
        }
        return $this->getId();
    }

    public function getGroup(){
        return $this->group;
    }

    public function setGroup(string $group){
        $this->group = $group;
    }

    public function getComment(){
        return $this->comment;
    }

    public function setComment(string $comment){
        $this->comment = trim($comment);
    }

    public function getSurvey(){
        return $this->survey;
    }

    public function setSurvey(Survey $survey){
        $this->survey = $survey;
    }

    public function getAlerts(){
        if (! $this->alerts){
            $this->setAlerts(array());
        }
        return $this->alerts;
    }

    public function setAlerts(array $alerts){
        $this->alerts = $alerts;
    }

    public function addAlert(Alert $alert){
        if (! $this->alerts){
            $this->setAlerts(array());
        }
        if (! $alert->getType()){
            throw new Exception("Subject ".$this->getId().": void alert");
        }
        $this->alerts[] = $alert;
    }

    public function addAlerts(array $alerts){
        foreach ($alerts as $alert){
            $this->addAlert($alert);
        }
    }

    public function createAlert(Alert $alert, array &$alerts){
        $this->addAlert($alert);
        $alerts[] = $alert;
        return $this;
    }

    public function isIncluded(){
        if ($this->getInitials()){
            return true;
        }
        return false;
    }
}
